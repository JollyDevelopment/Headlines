#pragma once

#include <chrono>
#include "util/HttpServer.h"
#include "posts_fwd.h"
#include "comments_fwd.h"
#include "api_fwd.h"

namespace util
{
    class WebSocket;
    class SimpleThread;
}

namespace api
{
    class Post;
    class Subreddit;
    class User;
    class Comment;
    class RedditAPI
    {
    public:
        static RedditAPI* Get();
        static void Destroy();

        RedditAPI();

        bool IsLoggedIn() const { return m_LoggedIn; }
        void Login();
        void Logout();
        void RefreshToken();
        UserRef GetCurrentUser();

        void Subscribe(const UserRef& user);
        void UnSubscribe(const UserRef& user);
        void Subscribe(const SubredditRef& subreddit);
        void UnSubscribe(const SubredditRef& subreddit);
        void Vote(const std::string& fullName, int dir);
        void Save(const std::string& fullName);
        void UnSave(const std::string& fullName);
        Json::Value Comment(const std::string& fullName, const std::string& text);
        void Delete(const std::string& fullName);

        void AddSubredditToMultiReddit(const SubredditRef& subreddit, const MultiRedditRef& multireddit);
        void RemoveSubredditFromMultiReddit(const SubredditRef& subreddit, const MultiRedditRef& multireddit);
        void AddUserToMultiReddit(const UserRef& user, const MultiRedditRef& multireddit);
        void RemoveUserFromMultiReddit(const UserRef& user, const MultiRedditRef& multireddit);
        api::MultiRedditRef CreateOrUpdateMultiReddit(const std::string& path,
                                       const std::string& name,
                                       const std::string& desc,
                                       bool isPrivate,
                                       const std::vector<std::string>& subscriptions);
        void DeleteMultiReddit(const MultiRedditRef& multiReddit);

        PostRef GetPost(const std::string& url);
        std::vector<PostRef> GetPosts(PostsType type, int limit, std::string& after);
        std::vector<PostRef> GetSearchPosts(const std::string& searchString, int limit, std::string& after);
        std::vector<PostRef> GetSubredditPosts(const SubredditRef& subreddit, PostsType type, int limit, std::string& after);
        std::vector<PostRef> GetUserPosts(const UserRef& user, PostsType type, int limit, std::string& after);
        std::vector<PostRef> GetUserSavedPosts(int limit, std::string& after);
        std::vector<PostRef> GetUserUpvotedPosts(int limit, std::string& after);
        std::vector<PostRef> GetUserDownvotedPosts(int limit, std::string& after);
        std::vector<PostRef> GetUserGildedPosts(int limit, std::string& after);
        std::vector<PostRef> GetUserHiddenPosts(int limit, std::string& after);
        std::vector<SubredditRef> GetUserSubreddits(int limit, std::string& after, bool clearCache);
        std::vector<MultiRedditRef> GetUserMultiReddits();
        std::vector<PostRef> GetMultiRedditPosts(const MultiRedditRef& multiReddit, PostsType type, int limit, std::string& after);

        std::vector<SubredditRef> GetSearchSubreddits(const std::string& searchString, int limit, std::string& after);
        std::vector<UserRef> GetSearchUsers(const std::string& searchString, int limit, std::string& after);
        UserRef GetUser(const std::string& username);

        SubredditRef GetSubreddit(const std::string& subreddit);
        std::vector<FlairRef> GetSubredditPostFlairs(const SubredditRef& subreddit);

        struct Comments
        {
            std::vector<CommentRef> m_Comments;
            std::vector<std::string> m_CommentsToLoad;
        };
        PostRef GetPostFromCommentsUrl(const std::string& url);
        Comments GetComments(CommentsType type, const std::string& permaLink, int limit, int context = -1, int depth = -1, const std::string& contextComment = "");
        Comments GetUserComments(const UserRef& user, CommentsType type, int limit, std::string& after);
        Comments GetUserSavedComments(int limit, std::string& after);
        std::vector<CommentRef>GetMoreComments(const std::string& linkId, int depth, const std::string& parentID, std::vector<std::string> commentIDs);

        std::vector<MessageRef> GetMessages(int limit, std::string& after);
        std::vector<MessageRef> GetSentMessages(int limit, std::string& after);
        std::vector<MessageRef> GetUnreadMessages();
        void ReadAllMessages();
        bool SendMessage(const std::string& to, const std::string& subject, const std::string& message, std::string& outError);

        PostRef SubmitTextPost(const SubredditRef& subreddit,
                               const std::string& title,
                               const std::string& text,
                               bool nsfw,
                               bool spoiler,
                               const FlairRef& flair,
                               std::string& outError);
        PostRef SubmitUrlPost(const SubredditRef& subreddit,
                              const std::string& title,
                              const std::string& url,
                              bool nsfw,
                              bool spoiler,
                              const FlairRef& flair,
                              std::string& outError);
        void SubmitImagePost(const SubredditRef& subreddit,
                             const std::string& title,
                             const std::string& url,
                             util::WebSocket* webSocket,
                             bool nsfw,
                             bool spoiler,
                             const FlairRef& flair,
                             const std::function<void(PostRef)>& onPosted,
                             std::string& outError);
        void SubmitVideoPost(const SubredditRef& subreddit,
                                const std::string& title,
                                const std::string& url,
                                const std::string& thumbnailUrl,
                                util::WebSocket* webSocket,
                                bool nsfw,
                                bool spoiler,
                                const FlairRef& flair,
                                const std::function<void(PostRef)>& onPosted,
                                std::string& outError);
        std::pair<std::string, util::WebSocket*> UploadMedia(const std::string& fileName);

    private:
        static RedditAPI* s_Instance;

        void GetUserInfo();

        std::vector<PostRef> GetPostsInternal(const std::string& url, const std::string& args, std::string& after);
        PostRef SubmitPostInternal(const std::string& url, std::string& outError);

        Json::Value ApiGet(const std::string& path, bool isApiCall, const std::string& args = "");
        Json::Value ApiPost(const std::string& path, const std::string& args);
        Json::Value ApiPut(const std::string& path, Json::Value args);
        Json::Value ApiDelete(const std::string& path);
        bool IsTokenExpired() const;

        void SaveToken();
        void LoadToken();

        util::HttpServer m_Server;
        std::string m_Token;
        std::string m_RefreshToken;
        uint64_t m_TokenExpiry;
        bool m_LoggedIn;
        std::string m_DisplayName;
        std::string m_UserUrl;
        UserRef m_CurrentUser;
        std::vector<SubredditRef> m_UserSubreddits;

        util::SimpleThread* m_GetCurrentUserThread;
        Glib::Dispatcher m_LoginLogoutMainThreadDispatcher;
    };
}
