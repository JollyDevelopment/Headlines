#include "User.h"
#include "IconCache.h"

namespace api
{
    User::User(Json::Value jsonData)
    {
        UpdateDataFromJson(std::move(jsonData));
    }

    void User::UpdateDataFromJson(Json::Value jsonValue)
    {
        m_Name = jsonValue["name"].asString();
        m_DisplayName = jsonValue["subreddit"]["display_name"].asString();
        m_DisplayNamePrefixed = jsonValue["subreddit"]["display_name_prefixed"].asString();
        m_IconPath = jsonValue["subreddit"]["icon_img"].asString();
        m_Karma = jsonValue["link_karma"].asInt() + jsonValue["comment_karma"].asInt();
        m_CakeDayTimeStamp = jsonValue["created_utc"].asUInt64();
        m_Url = jsonValue["subreddit"]["url"].asString();
        m_UserIsSubscriber = jsonValue["subreddit"]["user_is_subscriber"].asBool();

        if (!IconCache::Get()->IsUserIconCached(m_DisplayName))
        {
            IconCache::Get()->SetUserIcon(m_DisplayName, m_IconPath);
        }
    }

    void User::SetUserIsSubscriber(bool subscriber)
    {
        m_UserIsSubscriber = subscriber;
    }
}
