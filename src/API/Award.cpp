#include "Award.h"

namespace api
{
    Award::Award(const Json::Value& json)
    {
        m_ImageUrl = json["icon_url"].asString();
        m_Count = json["count"].asUInt();
        m_Description = json["description"].asString();
        m_Name = json["name"].asString();
    }
}