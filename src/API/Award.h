#pragma once

namespace api
{
    class Award
    {
    public:
        Award(const Json::Value& json);

        const std::string& GetImageUrl() const { return m_ImageUrl; };
        unsigned int GetCount() const { return m_Count; }
        const std::string& GetDescription() const { return m_Description; }
        const std::string& GetName() const { return m_Name; }

    private:
        std::string m_ImageUrl;
        unsigned int m_Count;
        std::string m_Description;
        std::string m_Name;
    };
}