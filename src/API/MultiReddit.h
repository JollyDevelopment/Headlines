#pragma once

#include "api_fwd.h"

namespace api
{
    class MultiReddit
    {
    public:
        enum class Visibility
        {
            Private,
            Public,
        };

        explicit MultiReddit(Json::Value jsonData);
        void SetUserIsSubscriber(bool userIsSubscriber);
        void AddSubreddit(const std::string& subreddit);
        void RemoveSubreddit(const std::string& subreddit);

        void SetFromMultiReddit(const MultiRedditRef& multiReddit);

        bool GetCanEdit() const { return m_CanEdit; }
        const std::string& GetDisplayName() const { return m_DisplayName; }
        const std::string& GetName() const { return m_Name; }
        const std::string& GetDescription() const { return m_Description; }
        const std::string& GetDescriptionHtml() const { return m_DescriptionHtml; }
        int GetNumSubs() const { return m_NumSubs; }
        const std::string& GetCopiedFrom() const { return m_CopiedFrom; }
        const std::string& GetIconUrl() const { return m_IconUrl; }
        const std::vector<std::string>& GetSubreddits() const { return m_Subreddits; }
        uint64_t GetCreatedUTC() const { return m_CreatedUTC; }
        uint64_t GetCreated() const { return m_Created; }
        Visibility GetVisibility() const { return m_Visibility; }
        bool GetOver18() const { return m_Over18; }
        const std::string& GetPath() const { return m_Path; }
        const std::string& GetOwner() const { return m_Owner; }
        bool GetSubscriber() const { return m_Subscriber; }
        const std::string& GetOwnerID() const { return m_OwnerID; }
        bool GetFavourited() const { return m_Favourited; }

    private:
        void UpdateDataFromJson(Json::Value jsonValue);

        bool m_CanEdit;
        std::string m_DisplayName;
        std::string m_Name;
        std::string m_DescriptionHtml;
        std::string m_Description;
        int m_NumSubs;
        std::string m_CopiedFrom;
        std::string m_IconUrl;
        std::vector<std::string> m_Subreddits;
        uint64_t m_CreatedUTC;
        uint64_t m_Created;
        Visibility m_Visibility;
        bool m_Over18;
        std::string m_Path;
        std::string m_Owner;
        bool m_Subscriber;
        std::string m_OwnerID;
        bool m_Favourited;
    };
}
