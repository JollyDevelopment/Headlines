#include "IconCache.h"

namespace api
{
    IconCache* IconCache::s_Instance = nullptr;

    IconCache* IconCache::Get()
    {
        if (!s_Instance)
        {
            s_Instance = new IconCache();
            s_Instance->Load();
        }

        return s_Instance;
    }

    bool IconCache::IsUserIconCached(const std::string& userName)
    {
        std::lock_guard<std::mutex> lock(m_Mutex);

        return m_UserIcons.count(userName) > 0;
    }

    std::string IconCache::GetUserIcon(const std::string& userName)
    {
        std::lock_guard<std::mutex> lock(m_Mutex);

        if (m_UserIcons.count(userName) > 0)
        {
            return m_UserIcons[userName];
        }

        return "";
    }

    void IconCache::SetUserIcon(const std::string& userName, const std::string& iconPath)
    {
        std::lock_guard<std::mutex> lock(m_Mutex);

        m_UserIcons[userName] = iconPath;
        Save();
    }

    bool IconCache::IsSubredditIconCached(const std::string& subredditName)
    {
        std::lock_guard<std::mutex> lock(m_Mutex);

        return m_SubredditIcons.count(subredditName) > 0;
    }

    std::string IconCache::GetSubredditIcon(const std::string& subredditName)
    {
        std::lock_guard<std::mutex> lock(m_Mutex);

        if (m_SubredditIcons.count(subredditName) > 0)
        {
            return m_SubredditIcons[subredditName];
        }

        return "";
    }

    void IconCache::SetSubredditIcon(const std::string& subredditName, const std::string& iconPath)
    {
        std::lock_guard<std::mutex> lock(m_Mutex);

        m_SubredditIcons[subredditName] = iconPath;
        Save();
    }

    void IconCache::Load()
    {
        std::ifstream ifstream;
        ifstream.open(GetFilePath());
        if (ifstream.is_open())
        {
            std::string lineValue;
            std::stringstream stringValue;
            while (!ifstream.eof())
            {
                ifstream >> lineValue;
                stringValue << lineValue;
            }

            Json::Value saveData = util::Helpers::GetJsonValueFromString(stringValue.str());

            Json::Value subredditData = saveData["subreddit_icons"];
            for (const std::string& member : subredditData.getMemberNames())
            {
                m_SubredditIcons[member] = subredditData[member].asString();
            }

            Json::Value userData = saveData["user_icons"];
            for (const std::string& member : userData.getMemberNames())
            {
                m_UserIcons[member] = userData[member].asString();
            }
        }
    }

    void IconCache::Save()
    {
        std::ofstream ofstream;
        ofstream.open(GetFilePath());

        Json::StreamWriterBuilder builder;
        Json::StreamWriter* writer = builder.newStreamWriter();

        Json::Value saveData;
        Json::Value subredditIconData = saveData["subreddit_icons"];
        for (const auto& [key, value] : m_SubredditIcons)
        {
            subredditIconData[key] = value;
        }
        Json::Value userIcons = saveData["user_icons"];
        for (const auto& [key, value] : m_SubredditIcons)
        {
            userIcons[key] = value;
        }

        writer->write(saveData, &ofstream);
        ofstream.close();
    }

    std::string IconCache::GetFilePath()
    {
        std::stringstream ss;
        ss << g_get_user_config_dir() << "/headlines/icon_cache.json";
        return ss.str();
    }
}