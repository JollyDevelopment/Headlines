#pragma once

namespace api
{
    class Message
    {
    public:
        Message(Json::Value jsonValue, const std::string& kind);

        unsigned int GetFirstMessage() const { return m_FirstMessage; }
        const std::string GetFirstMessageName() const { return m_FirstMessageName; }
        const std::string GetSubreddit() const { return m_Subreddit; }
        const std::string GetReplies() const { return m_Replies; }
        const std::string GetAuthorFullname() const { return m_AuthorFullname; }
        const std::string GetID() const { return m_ID; }
        const std::string GetSubject() const { return m_Subject; }
        int GetScore() const { return m_Score; }
        const std::string GetAuthor() const { return m_Author; }
        unsigned int GetNumComments() const { return m_NumComments; }
        const std::string GetParentID() const { return m_ParentID; }
        const std::string GetSubredditNamePrefixed() const { return m_SubredditNamePrefixed; }
        bool GetNew() const { return m_New; }
        const std::string GetType() const { return m_Type; }
        const std::string GetBody() const { return m_Body; }
        const std::string GetLinkTitle() const { return m_LinkTitle; }
        const std::string GetDest() const { return m_Dest; }
        bool GetWasComment() const { return m_WasComment; }
        const std::string GetBodyHtml() const { return m_BodyHtml; }
        const std::string GetName() const { return m_Name; }
        unsigned int GetCreated() const { return m_Created; }
        unsigned int GetCreatedUTC() const { return m_CreatedUTC; }
        const std::string GetContext() const { return m_Context; }
        bool GetIsModerator() const { return m_IsModerator; }
        const std::string GetKind() const { return m_Kind; }

    private:
        void UpdateDataFromJson(Json::Value jsonValue);

        unsigned int m_FirstMessage;
        std::string m_FirstMessageName;
        std::string m_Subreddit;
        std::string m_Replies;
        std::string m_AuthorFullname;
        std::string m_ID;
        std::string m_Subject;
        int m_Score;
        std::string m_Author;
        unsigned int m_NumComments;
        std::string m_ParentID;
        std::string m_SubredditNamePrefixed;
        bool m_New;
        std::string m_Type;
        std::string m_Body;
        std::string m_LinkTitle;
        std::string m_Dest;
        bool m_WasComment;
        std::string m_BodyHtml;
        std::string m_Name;
        unsigned int m_Created;
        unsigned int m_CreatedUTC;
        std::string m_Context;
        bool m_IsModerator;
        std::string m_Kind;
    };
}
