#pragma once

namespace api
{
    enum class CommentsType
    {
        Confidence,
        Top,
        New,
        Controversial,
        Old,
        Random,
        Qa,
        Live
    };

    inline std::string CommentsTypeToString(CommentsType type)
    {
        switch (type)
        {
            case CommentsType::Confidence:
            {
                return "Best";
            }
            case CommentsType::Top:
            {
                return "Top";
            }
            case CommentsType::New:
            {
                return "New";
            }
            case CommentsType::Controversial:
            {
                return "Controversial";
            }
            case CommentsType::Old:
            {
                return "Old";
            }
            case CommentsType::Random:
            {
                return "Random";
            }
            case CommentsType::Qa:
            {
                return "Qa";
            }
            case CommentsType::Live:
            {
                return "Live";
            }
        }

        return "";
    }
}
