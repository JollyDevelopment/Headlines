#include "Colour.h"

namespace util
{
    Colour::Colour()
        : m_R(0)
        , m_G(0)
        , m_B(0)
    {
    }

    Colour::Colour(double r, double g, double b)
        : m_R(r)
        , m_G(g)
        , m_B(b)
    {
    }

    Colour::Colour(unsigned int hex)
        : m_R(((hex >> 16) & 0xFF) / 255.0)
        , m_G(((hex >> 8) & 0xFF) / 255.0)
        , m_B(((hex) & 0xFF) / 255.0)
    {
    }

    Pango::AttrColor Colour::AsPangoForegroundColour() const
    {
        return Pango::AttrColor::create_attr_foreground((guint16)(m_R * 65535), (guint16)(m_G * 65535), (guint16)(m_B * 65535));
    }

    Pango::AttrColor Colour::AsPangoBackgroundColour() const
    {
        return Pango::AttrColor::create_attr_background((guint16)(m_R * 65535), (guint16)(m_G * 65535), (guint16)(m_B * 65535));
    }
}