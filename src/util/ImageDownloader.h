#pragma once

#include "API/Post.h"

namespace util
{
    class SimpleThread;
    class ThreadWorker;
    class ImageDownloader
    {
    public:
        void DownloadImageAsync(const util::ImageData& imageData, std::function<void(const std::string&)> onFinished);
        void Cancel();
    private:
        void DownloadImageInternal(util::ThreadWorker* worker);

        util::ImageData m_ImageData;
        std::function<void(std::string)> m_OnFinished = nullptr;
        util::SimpleThread* m_ImageDownloadThread = nullptr;
    };
}
