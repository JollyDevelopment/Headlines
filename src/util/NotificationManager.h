#pragma once
#include "API/api_fwd.h"

namespace util
{
    class SimpleThread;
    class NotificationManager
    {
    public:
        static NotificationManager* Get();

        NotificationManager();
        ~NotificationManager();

        unsigned int GetNumUnreadMessages() { return m_UnreadMessages.size(); }

        sigc::signal<void(unsigned int)>& SignalNewMessages() { return m_SignalNewMessages; }

        void MarkMessagesRead();

    private:
        void GetUnreadMessages();

        void Save();
        void Load();
        static std::string GetFilePath();

        Json::Value m_SaveData;
        sigc::connection m_timeout_connection;
        util::SimpleThread* m_UnreadNotificationsThread;
        std::vector<api::MessageRef> m_UnreadMessages;
        sigc::signal<void(unsigned int)> m_SignalNewMessages;
    };
}
