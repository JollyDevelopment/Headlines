#include "pch.h"

#if WIN32
#include <Windows.h>
#endif

#if !DIST_BUILD
bool IsDebuggerAttached()
{
#if WIN32
    return IsDebuggerPresent();
#else
    return false; // TODO
#endif
}
#endif