#pragma once

#include "Page.h"
#include "UI/ContentProviders/SubredditPostsContentProvider.h"
#include "API/api_fwd.h"
#include "util/Signals.h"

namespace util
{
    class SimpleThread;
}

namespace api
{
    class Subreddit;
}

namespace ui
{
    class ImageWidget;
    class SubredditPage : public Page
    {
    public:
        explicit SubredditPage(const api::SubredditRef& subreddit);
        explicit SubredditPage(const std::string& subredditName);
        Gtk::Box* CreateUIInternal(AdwLeaflet* parent) override;
        virtual void Cleanup() override;
        virtual void Reload() override;
        virtual SortType GetSortType() const override { return SortType::SubredditPosts; }
        virtual UISettings GetUISettings() const override;

        ::api::SubredditRef GetSubreddit() { return m_Subreddit; }

    private:
        void SetupActions();
        void OnHeaderBarCreated() override;
        void SetSubreddit(const api::SubredditRef& subreddit);

        AdwViewStack* m_Stack;
        AdwViewSwitcherBar* m_SwitcherBar;
        Gtk::Box* m_Sidebar = nullptr;

        ::api::SubredditRef m_Subreddit = nullptr;
        SubredditWidgetRef m_PostsSubredditWidget = nullptr;
        ui::SubredditPostsContentProviderRef m_ContentProvider;
        ui::RedditContentListViewRef m_PostsView;
        SubredditWidgetRef m_SidebarSubredditWidget = nullptr;

        std::vector<util::SimpleThread*> m_Threads;
        Gtk::Spinner* m_Spinner = nullptr;

        int m_RoundedCornersSettingChangedHandler = util::s_InvalidSignalHandlerID;
    };
}
