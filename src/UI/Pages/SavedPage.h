#pragma once

#include "util/Signals.h"
#include "Page.h"
#include "UI/CommentsView.h"

namespace ui
{
    class SavedPage : public Page
    {
    public:
        SavedPage();
        Gtk::Box* CreateUIInternal(AdwLeaflet* parent) override;
        virtual void Cleanup() override;
        virtual void Reload() override;
        virtual SortType GetSortType() const override;
        virtual UISettings GetUISettings() const override;

        void ReloadPosts();
        void ReloadComments();

    private:
        void OnHeaderBarCreated() override;

        AdwViewStack* m_Stack = nullptr;
        AdwViewSwitcherBar* m_SwitcherBar = nullptr;
        CommentsView m_CommentsView;
        RedditContentListViewRef m_SavedPostsView;
    };
}
