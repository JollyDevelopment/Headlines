#pragma once

#include "Page.h"
#include "API/api_fwd.h"
#include "API/Post.h"
#include "UI/CommentsView.h"
#include "util/Signals.h"
#include "UI/Widgets/UserWidget.h"

namespace api
{
    class User;
}

namespace ui
{
    class ImageWidget;
    class UserPage : public Page
    {
    public:
        UserPage();
        explicit UserPage(const api::UserRef& user);
        explicit UserPage(const std::string& username);
        Gtk::Box* CreateUIInternal(AdwLeaflet* parent) override;
        virtual void Cleanup() override;
        virtual void Reload() override;
        virtual SortType GetSortType() const override;
        virtual std::vector<SortType> GetAllSortTypes() const override;
        virtual UISettings GetUISettings() const override;

        void VisibleChildChanged();

    private:
        void OnHeaderBarCreated() override;
        void SetUser(const ::api::UserRef& subreddit);
        void SetupActions();

        AdwViewStack* m_Stack;
        AdwViewSwitcherBar* m_SwitcherBar;
        guint m_StackPageSwitchConnection;

        ::api::UserRef m_User = nullptr;
        UserWidgetRef m_PostsUserWidget = nullptr;
        UserPostsContentProviderRef m_PostsContentProvider;
        RedditContentListViewRef m_PostsView = nullptr;
        UserWidgetRef m_CommentsUserWidget = nullptr;
        CommentsViewRef m_CommentsView = nullptr;

        std::vector<util::SimpleThread*> m_Threads;
        Gtk::Spinner* m_Spinner = nullptr;
    };
}
