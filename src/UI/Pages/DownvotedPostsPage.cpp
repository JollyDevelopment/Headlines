#include "DownvotedPostsPage.h"
#include "AppSettings.h"
#include "UI/ListViews/RedditContentListView.h"
#include "UI/ContentProviders/UserDownvotedPostsContentProvider.h"

namespace ui
{
    DownvotedPostsPage::DownvotedPostsPage()
        : Page(PageType::DownvotedPage)
        , m_ListView(std::make_shared<ui::RedditContentListView>(std::make_shared<UserDownvotedPostsContentProvider>()))
    {

    }

    void DownvotedPostsPage::Cleanup()
    {
    }

    void DownvotedPostsPage::Reload()
    {
        m_ListView->ClearContent();
        m_ListView->LoadContentAsync();
    }

    Gtk::Box*  DownvotedPostsPage::CreateUIInternal(AdwLeaflet* parent)
    {
        auto builder = Gtk::Builder::create_from_resource("/io/gitlab/caveman250/headlines/ui/posts_view.ui");

        Gtk::Box* box = builder->get_widget<Gtk::Box>("PostsView");
        m_LeafletPage = adw_leaflet_append(parent, (GtkWidget*)box->gobj());
        adw_leaflet_page_set_name(m_LeafletPage, GetID().c_str());

        Gtk::Viewport* postsViewport;
        postsViewport = builder->get_widget<Gtk::Viewport>("PostsViewport");
        Gtk::Box* postsBox;
        postsBox = builder->get_widget<Gtk::Box>("PostsBox");
        m_ListView->CreateUI(postsBox, postsViewport);
        m_ListView->LoadContentAsync();

        return box;
    }

    UISettings DownvotedPostsPage::GetUISettings() const
    {
        return UISettings
        {
            true,
            false,
            false,
            true,
            false,
            true,
            false,
            false
        };
    }
}