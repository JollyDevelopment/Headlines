#pragma once

#include "Page.h"
#include "UI/ui_types_fwd.h"

namespace ui
{
    class DownvotedPostsPage : public Page
    {
    public:
        DownvotedPostsPage();
        Gtk::Box* CreateUIInternal(AdwLeaflet* parent) override;
        virtual void Cleanup() override;
        virtual void Reload() override;
        virtual SortType GetSortType() const override { return SortType::UserPosts; }
        virtual UISettings GetUISettings() const override;
    private:

        ui::RedditContentListViewRef m_ListView;
    };
}