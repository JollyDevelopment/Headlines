#pragma once

#include "Page.h"

namespace ui
{
    class GildedPostsPage : public Page

    {
    public:
        GildedPostsPage();
        Gtk::Box* CreateUIInternal(AdwLeaflet* parent) override;
        virtual void Cleanup() override;
        virtual void Reload() override;
        virtual SortType GetSortType() const override { return SortType::UserPosts; }
        virtual UISettings GetUISettings() const override;
    private:

        ui::RedditContentListViewRef m_ListView;
    };
}