#pragma once

#include "API/api_fwd.h"

namespace ui
{
    class MultiRedditBanner
    {
    public:
        ~MultiRedditBanner();

        Gtk::Box* CreateUI(Gtk::Box* parent, const api::MultiRedditRef& multi);
        void OnMultiUpdated(const api::MultiRedditRef& multi);

    private:
        Gtk::Label* m_NameLabel = nullptr;
        Gtk::Box* m_DescBox = nullptr;

        int m_RoundedCornersSettingChangedHandler = util::s_InvalidSignalHandlerID;
    };
}