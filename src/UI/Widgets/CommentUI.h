#pragma once

#include "UI/ui_types_fwd.h"
#include "UI/CommentsView.h"

namespace api
{
    class Comment;
}

namespace ui
{
    class CommentUI
    {
    public:
        CommentUI(api::CommentRef comment, CommentsViewType commentsViewType, int depth);
        ~CommentUI();
        void CreateUI(Gtk::Box* parent, int index = -1);

        Gtk::Box* GetBox() { return m_Box; }
        const api::CommentRef& GetComment() { return m_Comment; }
        bool HasAddedUI() const { return m_HasAddedUI; }
        void OnParentCommentDestroyed();

    private:
        void SetupCommentText(const Glib::RefPtr<Gtk::Builder>& builder);
        void SetupUsername(const Glib::RefPtr<Gtk::Builder>& builder);
        void SetupDate(const Glib::RefPtr<Gtk::Builder>& builder);
        void SetupVoteButtons(const Glib::RefPtr<Gtk::Builder>& builder);
        void SetupBookmarkButton(const Glib::RefPtr<Gtk::Builder>& builder);
        void SetupDeleteButton(const Glib::RefPtr<Gtk::Builder>& builder);
        void SetupReplyButton(const Glib::RefPtr<Gtk::Builder>& builder);
        void SetupShareButton(const Glib::RefPtr<Gtk::Builder>& builder);
        void SetupCollapseButton(const Glib::RefPtr<Gtk::Builder>& builder);
        void SetupDrawingArea(const Glib::RefPtr<Gtk::Builder>& builder);
        void SetupChildren(const Glib::RefPtr<Gtk::Builder>& builder);
        void SetupSecondRow(const Glib::RefPtr<Gtk::Builder>& builder);
        void AddMoreCommentsItem();
        void ToggleChildren();

        void LoadMoreCommentsAsync();
        static void* LoadMoreComments(void* args);
        void LoadMoreComments();

        void UpdateScoreLabel();

        void UpVote();
        void RemoveUpVote();
        void DownVote();
        void RemoveDownVote();

        Gtk::Box* m_Box;
        Gtk::Box* m_CommentTextBox;
        Gtk::Label* m_UsernameLabel;
        Gtk::Box* m_ChildrenBox;
        Gtk::Button* m_CollapseButton;
        Gtk::Button* m_UnCollapseButton;
        Gtk::Box* m_MoreCommentsBox;
        Glib::Dispatcher m_MoreCommentsUIDispatcher;
        Gtk::Label* m_ScoreLabel;
        Gtk::Image* m_UpVoteImage;
        Gtk::Image* m_DownVoteImage;
        Gtk::Image* m_BookmarkImage;
        Gtk::Spinner* m_Spinner;
        Gtk::Button* m_ShareButton;
        Gtk::Label* m_DateLabel;
        Gtk::Box* m_CommentSecondRow;

        api::CommentRef m_Comment;
        CommentsViewType m_CommentsViewType;
        int m_Depth;
        bool m_ChildrenVisible;
        std::vector<CommentWidgetRef> m_Children;
        bool m_HasAddedUI;
        int m_RoundedCornersSettingChangedHandler;
    };
}
