#pragma once

#include "RedditContentListItem.h"
#include "UI/ui_types_fwd.h"
#include "API/api_fwd.h"
#include "util/Colour.h"

namespace util
{
    class SimpleThread;
}

namespace api
{
    class Post;
}

namespace ui
{
    class PictureWidget;
    class PostWidget : public RedditContentListItem
    {
    public:
        explicit PostWidget(const api::PostRef& post);
        PostWidget(const api::PostRef& post, const ui::CommentsViewRef& commentsView);
        ~PostWidget();
        virtual void CreateUI(Gtk::Widget* parent) override;
        virtual void RemoveUI() override;
        Gtk::Widget* GetWidget() override { return m_Card; }
        virtual int GetContentTop() override;
        virtual int GetContentBottom() override;
        virtual void SetActive(bool active) override;
    private:
        static bool IsValidImageUrl(const std::string& imageUrl) ;

        void UpVote();
        void RemoveUpVote();
        void DownVote();
        void RemoveDownVote();
        void UpdateScoreLabel();

        // UI Setup
        void SetupCard(const Glib::RefPtr<Gtk::Builder>& builder);
        void SetupSubredditIcon(const Glib::RefPtr<Gtk::Builder>& builder);
        void SetupSubredditLabel(const Glib::RefPtr<Gtk::Builder>& builder);
        void SetupUserLabel(const Glib::RefPtr<Gtk::Builder>& builder);
        void SetupFlairs(const Glib::RefPtr<Gtk::Builder>& builder);
        void SetupTitle(const Glib::RefPtr<Gtk::Builder>& builder);
        void SetupPostContents(const Glib::RefPtr<Gtk::Builder>& builder);
        void SetupSelfText();
        void SetupImage();
        void SetupPostActions(const Glib::RefPtr<Gtk::Builder>& builder);
        void SetupVoteButtons(const Glib::RefPtr<Gtk::Builder>& builder);
        void SetupCommentsButton(const Glib::RefPtr<Gtk::Builder>& builder);
        void SetupShareButton(const Glib::RefPtr<Gtk::Builder>& builder);
        void SetupBookmarkButton(const Glib::RefPtr<Gtk::Builder>& builder);
        void SetupReplyButton(const Glib::RefPtr<Gtk::Builder>& builder);
        void SetupDeleteButton(const Glib::RefPtr<Gtk::Builder>& builder);
        void AddCustomFlair(const std::string& text, const util::Colour& colour, const util::Colour& textColour);
        void SetupDateText(const Glib::RefPtr<Gtk::Builder>& builder);
        void SetupAwards(const Glib::RefPtr<Gtk::Builder>& builder);

        api::PostRef m_Post;
        ui::CommentsViewRef m_CommentsView;

        Glib::Dispatcher* m_MainThreadDispatcher;
        Gtk::Box* m_Card;
        PictureWidget* m_SubredditIcon = nullptr;
        PictureWidget* m_PostImage = nullptr;
        Gtk::Box* m_FlairBox = nullptr;
        Gtk::Box* m_AwardsBox = nullptr;
        std::vector<FlairWidgetRef> m_Flairs;
        Gtk::Box* m_ContentsBox = nullptr;
        Gtk::Label* m_ScoreLabel = nullptr;
        Gtk::Image* m_UpVoteImage = nullptr;
        Gtk::Image* m_DownVoteImage = nullptr;
        Gtk::Image* m_BookmarkImage = nullptr;

        util::SimpleThread* m_SubredditIconThread = nullptr;

        int m_RoundedCornersSettingChangedHandler;
    };
}
