#pragma once
#include "RedditContentListItem.h"
#include "UI/ui_types_fwd.h"
#include "API/api_fwd.h"

namespace api
{
    class User;
    class Subreddit;
}

namespace ui
{
    class PictureWidget;
    class SearchResultWidget : public RedditContentListItem
    {
    public:
        SearchResultWidget(const api::UserRef& user, std::function<void(const SearchResultWidget*)> onClicked);
        SearchResultWidget(const api::SubredditRef& subreddit, std::function<void(const SearchResultWidget*)> onClicked);
        SearchResultWidget(const api::MultiRedditRef& multiReddit, std::function<void(const SearchResultWidget*)> onClicked);
        ~SearchResultWidget();
        virtual void CreateUI(Gtk::Widget* parent) override;
        virtual void RemoveUI() override;
        virtual int GetContentTop() override;
        virtual int GetContentBottom() override;
        Gtk::Widget* GetWidget() override { return m_ListBoxRow ? (Gtk::Widget*)m_ListBoxRow : m_Box; }
        virtual void OnActivate() override { m_OnClicked(this); }

        void AddActionButton(const std::string& iconName, std::function<void()> cb);

        void SetSubreddit(const api::SubredditRef& subreddit);

        const api::SubredditRef& GetSubreddit() const { return m_Subreddit; }
        const api::UserRef& GetUser() const { return m_User; }
        const api::MultiRedditRef& GetMultiReddit() const { return m_MultiReddit; }

    private:
        std::string m_ImageUrl;
        std::string m_DisplayName;
        api::UserRef m_User;
        api::SubredditRef m_Subreddit;
        api::MultiRedditRef m_MultiReddit;
        PictureWidget* m_Icon;
        Gtk::ListBoxRow* m_ListBoxRow = nullptr;
        Gtk::Box* m_Box;
        Gtk::Label* m_Label;

        std::function<void(const SearchResultWidget*)> m_OnClicked;
    };
}
