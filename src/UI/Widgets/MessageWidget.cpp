#include "MessageWidget.h"

#include <util/Signals.h>
#include <util/SimpleThread.h>
#include "FlairWidget.h"
#include <API/Flair.h>
#include <AppSettings.h>
#include <API/RedditAPI.h>
#include <util/ImageDownloader.h>
#include <Application.h>
#include <util/HtmlParser.h>
#include "PictureWidget.h"
#include "API/Message.h"
#include "API/IconCache.h"
#include "UI/Popups/SendMessagePopup.h"
#include "util/NotificationManager.h"
#include "API/User.h"

namespace ui
{
    MessageWidget::MessageWidget(const api::MessageRef& message)
        : m_Message(message)
    {

    }

    MessageWidget::~MessageWidget()
    {
        if (m_RoundedCornersSettingChangedHandler != util::s_InvalidSignalHandlerID)
        {
            util::Signals::Get()->RoundedCornersSettingChanged.RemoveHandler(m_RoundedCornersSettingChangedHandler);
        }

        if (m_IconThread)
        {
            m_IconThread->Cancel();
            m_IconThread = nullptr;
        }
    }

    void MessageWidget::CreateUI(Gtk::Widget* parent)
    {
        auto builder = Gtk::Builder::create_from_resource("/io/gitlab/caveman250/headlines/ui/message.ui");

        SetupCard(builder);
        SetupIcon(builder);
        SetupBadges(builder);
        SetupTitleLabel(builder);
        SetupContents(builder);
        SetupSourceLabel(builder);
        SetupDateText(builder);

        ((Gtk::Box*)parent)->append(*m_Card);
    }

    void MessageWidget::RemoveUI()
    {
        if (m_Card)
        {
            Gtk::Box *box = (Gtk::Box *) m_Card->get_parent();
            box->remove(*m_Card);
            m_Card = nullptr;
        }
    }

    int MessageWidget::GetContentTop()
    {
        return m_Card->get_allocation().get_y();
    }

    int MessageWidget::GetContentBottom()
    {
        const Gtk::Allocation& allocation = m_Card->get_allocation();
        return allocation.get_y() + allocation.get_height();
    }

    void MessageWidget::SetActive(bool active)
    {
        if (active != m_Active)
        {
            m_Icon->SetActive(active);
            m_Card->set_child_visible(active);
        }

        RedditContentListItem::SetActive(active);
    }

    void MessageWidget::SetupCard(const Glib::RefPtr<Gtk::Builder>& builder)
    {
        m_Card = builder->get_widget<Gtk::Box>("Message");

        util::Helpers::ApplyCardStyle(m_Card, AppSettings::Get()->GetBool("rounded_corners", false));
        m_RoundedCornersSettingChangedHandler = util::Signals::Get()->RoundedCornersSettingChanged.AddHandler([this](bool roundedCorners)
        {
            util::Helpers::ApplyCardStyle(m_Card, roundedCorners);
        });

        Glib::RefPtr<Gtk::GestureClick> gestureClick = Gtk::GestureClick::create();
        m_Card->add_controller(gestureClick);
        gestureClick->signal_released().connect([this](int n_press, double, double)
        {
            if (n_press == 0)
            {
                return;
            }

            if (!m_Message->GetContext().empty())
            {
                util::Helpers::HandleLink(m_Message->GetContext());
            }
            else if (m_Message->GetKind() == "t4" && m_Message->GetAuthor() != api::RedditAPI::Get()->GetCurrentUser()->GetName())
            {
                ui::SendMessagePopupRef sendMessagePopup =
                        std::make_shared<ui::SendMessagePopup>(m_Message->GetAuthor(),
                                                               util::Helpers::FormatString("Re: %s", m_Message->GetSubject().c_str()));

                Application::Get()->AddPage(sendMessagePopup);
            }
        });
    }

    void MessageWidget::SetupIcon(const Glib::RefPtr<Gtk::Builder>& builder)
    {
        Gtk::Box *subIconBox = builder->get_widget<Gtk::Box>("SubredditPictureBox");

        m_Icon = new PictureWidget(32, 32, false, false);
        m_Icon->set_size_request(32, 32);
        subIconBox->append(*m_Icon);

        m_IconThread = new util::SimpleThread([this](util::ThreadWorker *worker)
        {
            if (worker->IsCancelled())
            {
                return;
            }

            if (!m_Message->GetAuthor().empty())
            {
                if (!api::IconCache::Get()->IsUserIconCached(util::Helpers::FormatString("u_%s", m_Message->GetAuthor().c_str())))
                {
                    api::RedditAPI::Get()->GetUser(m_Message->GetAuthor());
                }
            }
        },
        [this](bool cancelled, util::SimpleThread*)
        {
            if (!cancelled)
            {
                if (m_Icon)
                {
                    std::string iconPath;
                    if (!m_Message->GetAuthor().empty())
                    {
                        iconPath = api::IconCache::Get()->GetUserIcon(util::Helpers::FormatString("u_%s", m_Message->GetAuthor().c_str()));
                    }

                    if (!iconPath.empty())
                    {
                        m_Icon->SetImageData(util::ImageData(util::Helpers::UnescapeHtml(iconPath)));
                    }
                    else
                    {
                        m_Icon->SetFromResource("/io/gitlab/caveman250/headlines/default_subreddit.png");
                    }
                }

                m_IconThread = nullptr; //thread deletes itself.
            }
        });
    }

    void MessageWidget::SetupTitleLabel(const Glib::RefPtr<Gtk::Builder>& builder)
    {
        Gtk::Label* titleLabel;
        titleLabel = builder->get_widget<Gtk::Label>("TitleLabel");
        titleLabel->set_label(m_Message->GetSubject());
    }

    void MessageWidget::SetupSourceLabel(const Glib::RefPtr<Gtk::Builder>& builder)
    {
        Gtk::Label* sourceLabel;
        sourceLabel = builder->get_widget<Gtk::Label>("SourceLabel");

        if (!m_Message->GetAuthor().empty())
        {
            sourceLabel->set_text(util::Helpers::FormatString("u/%s", m_Message->GetAuthor().c_str()));
            sourceLabel->add_css_class("username_text");
        }
    }

    void MessageWidget::SetupBadges(const Glib::RefPtr<Gtk::Builder>& builder)
    {
        m_BadgeBox = builder->get_widget<Gtk::Box>("BadgeBox");

        if (m_Message->GetNew())
        {
            AddCustomFlair("New!", util::Colour(0.1, 0.78, 0.26), util::Colour(1, 1, 1));
        }

        if (m_Message->GetKind() == "t4")
        {
            AddCustomFlair("Message", util::Colour(0.1, 0.26, 0.98), util::Colour(1, 1, 1));
        }
        else if (m_Message->GetType() == "comment_reply")
        {
            AddCustomFlair("Comment Reply", util::Colour(0.1, 0.26, 0.98), util::Colour(1, 1, 1));
        }
        else if (m_Message->GetType() == "post_reply")
        {
            AddCustomFlair("Post Reply", util::Colour(0.1, 0.26, 0.98), util::Colour(1, 1, 1));
        }
        else
        {
            ASSERT(false, "MessageWidget::SetupBadges - Unhandled message type %s for kind %s", m_Message->GetType().c_str(), m_Message->GetKind().c_str());
            AddCustomFlair(m_Message->GetType(), util::Colour(0.1, 0.26, 0.98), util::Colour(1, 1, 1));
        }
    }

    void MessageWidget::SetupContents(const Glib::RefPtr<Gtk::Builder>& builder)
    {
        m_ContentsBox = builder->get_widget<Gtk::Box>("ContentsBox");

        util::HtmlParser htmlParser;
        htmlParser.ParseHtml(m_Message->GetBodyHtml(), m_ContentsBox, -1, true);
    }

    void MessageWidget::AddCustomFlair(const std::string& text, const util::Colour& colour, const util::Colour& textColour)
    {
        api::FlairRef flair = std::make_shared<api::Flair>(text, colour, textColour);
        ui::FlairWidgetRef flairUI = std::make_shared<FlairWidget>(flair);
        flairUI->CreateUI(m_BadgeBox);
        m_Badges.push_back(flairUI);
    }

    void MessageWidget::SetupDateText(const Glib::RefPtr<Gtk::Builder>& builder)
    {
        Gtk::Label* dateLabel;
        dateLabel = builder->get_widget<Gtk::Label>("DateText");
        dateLabel->set_text(util::Helpers::TimeStampToTimeAndDateString(m_Message->GetCreated()));
    }
}