#pragma once

#include "Popup.h"
#include "API/api_fwd.h"
#include "util/Signal.h"

namespace util
{
    class SimpleThread;
}
namespace ui
{
    class EditMultiPopup : public Popup
    {
    public:
        explicit EditMultiPopup(const api::MultiRedditRef& multireddit);
        virtual void Cleanup() override;
        virtual void Reload() override {}
        virtual SortType GetSortType() const override { return SortType::None; }
        virtual UISettings GetUISettings() const override;

    private:
        void OnPopupChoiceSelected(GtkResponseType response) override;
        Gtk::Box* CreateUIInternal(AdwLeaflet* parent) override;

        void GetSubredditsAsync();
        void FixEntryRowSpacing(Gtk::Widget* entryWidget);

        void CreateSubredditListItem(const api::SubredditRef& subreddit);
        void CreateUserListItem(const api::UserRef& user);

        bool Validate();
        void UpdateConfirmBtn();
        void Confirm();

        void AddSubscription(std::string subscriptionName);
        void RemoveSubscription(std::string subscriptionName);

        util::SimpleThread* m_Thread;
        api::MultiRedditRef m_MultiReddit;
        ui::UserSubredditsPopupRef m_SubsPage = nullptr;
        Gtk::Spinner* m_Spinner = nullptr;
        Gtk::ListBox* m_ListBox = nullptr;
        Gtk::ListBox* m_SubredditsListBox = nullptr;
        std::vector<api::SubredditRef> m_Subreddits;
        std::vector<api::UserRef> m_Users;
        Gtk::Entry* m_NameEntry = nullptr;
        Gtk::Entry* m_DescEntry = nullptr;
        bool m_Private;
        int m_ListRoundedCornersSettingChangedHandler = util::s_InvalidSignalHandlerID;

        std::vector<std::string> m_PendingSubreddits;
    };
}
