#include "SendMessagePopup.h"
#include "Application.h"
#include "UserSubredditsPopup.h"
#include "util/ThreadWorker.h"
#include "UI/ContentProviders/UserSubredditsContentProvider.h"
#include "API/RedditAPI.h"
namespace ui
{
    SendMessagePopup::SendMessagePopup()
        : Popup(PageType::SendMessage, false)
    {

    }

    SendMessagePopup::SendMessagePopup(const std::string& username, const std::string& title)
        : Popup(PageType::SendMessage, false)
        , m_DefaultRecipient(username)
        , m_DefaultTitle(title)
    {

    }

    void SendMessagePopup::OnPopupChoiceSelected(GtkResponseType response)
    {
        switch (response)
        {
            case GTK_RESPONSE_ACCEPT:
                TrySendMessage();
                break;
            case GTK_RESPONSE_CANCEL:
                Close();
                break;
            default:
                g_warning("SendMessagePopup::OnPopupChoiceSelected - Unhandled response type: %d", response);
                break;
        }
    }

    void SendMessagePopup::TrySendMessage()
    {
        std::string error;
        if (!api::RedditAPI::Get()->SendMessage(m_RecipientEntry->get_text(), m_TitleEntry->get_text(), m_TextView->get_buffer()->get_text(), error))
        {
            ShowErrorMessage(error);
        }
        else
        {
            Close();
        }
    }

    void SendMessagePopup::CheckValid()
    {
        SetHeaderButtonEnabled(GTK_RESPONSE_ACCEPT, m_RecipientEntry->get_text_length() > 0 && m_TextView->get_buffer()->size() > 0 && m_TitleEntry->get_text_length() > 0);
    }

    void SendMessagePopup::ShowErrorMessage(const std::string& errorMsg)
    {
        Gtk::MessageDialog* confirmDialog = new Gtk::MessageDialog(errorMsg, false, Gtk::MessageType::ERROR, Gtk::ButtonsType::OK);
        confirmDialog->signal_response().connect([confirmDialog](int)
        {
            delete confirmDialog;
        });

        confirmDialog->set_transient_for(GetDialog() ? *GetDialog() : *Application::Get()->GetGtkApplication()->get_active_window());
        confirmDialog->show();
    }

    Gtk::Box* SendMessagePopup::CreateUIInternal(AdwLeaflet* parent)
    {
        AddPopupAction(PopupAction{ "Cancel", GTK_RESPONSE_CANCEL, true, [this]()
        {
            Close();
        }});
        AddPopupAction(PopupAction{ "Send", GTK_RESPONSE_ACCEPT, false, [this]()
        {
            TrySendMessage();
        }});

        auto builder = Gtk::Builder::create_from_resource("/io/gitlab/caveman250/headlines/ui/send_message.ui");
        Gtk::Box* box = builder->get_widget<Gtk::Box>("Box");

        m_RecipientEntry = builder->get_widget<Gtk::Entry>("RecipientEntry");
        if (!m_DefaultRecipient.empty())
        {
            m_RecipientEntry->set_text(m_DefaultRecipient);
        }
        m_RecipientEntry->signal_changed().connect([this]()
                                                   {
                                                       CheckValid();
                                                   });

        m_TitleEntry = builder->get_widget<Gtk::Entry>("TitleEntry");
        if (!m_DefaultTitle.empty())
        {
            m_TitleEntry->set_text(m_DefaultTitle);
        }
        m_TitleEntry->signal_changed().connect([this]()
                                               {
                                                   CheckValid();
                                               });

        m_TextView = builder->get_widget<Gtk::TextView>("TextView");
        m_TextView->get_buffer()->signal_changed().connect([this]()
                                                           {
                                                               CheckValid();
                                                           });

        CreatePopup(parent, box, "Send Message");

        return box;
    }
}
