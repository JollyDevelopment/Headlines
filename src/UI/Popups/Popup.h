#pragma once

#include "UI/Pages/Page.h"

namespace ui
{
    struct PopupAction
    {
        std::string m_Label;
        GtkResponseType m_ResponseType;
        bool m_EnabledByDefault;
        std::function<void()> m_Callback;
    };

    class Popup : public Page
    {
    public:
        explicit Popup(PageType pageType, bool createAsPage);
        virtual ~Popup();
        void CreateUI(AdwLeaflet* parent) override;
        bool IsPopup() const override { return true; }
        bool CreatedAsPage() const { return m_ForceCreateAsPage; }
        Gtk::Dialog* GetDialog() const { return m_Dialog; }
        void Close();

    protected:
        void AddPopupAction(PopupAction action);
        void Show();
        void SetHeaderButtonEnabled(GtkResponseType responseType, bool enabled);

        void OnHeaderBarCreated() override;

        virtual void OnPopupChoiceSelected(GtkResponseType response) = 0;

        void CreatePopup(AdwLeaflet* parent, Gtk::Box* box, const std::string& title);

    private:
        Gtk::Dialog* m_Dialog = nullptr;
        sigc::connection m_DialogResponseConnection;
        std::vector<PopupAction> m_Actions;
        std::map<GtkResponseType, Gtk::Button*> m_HeaderButtons;
        std::string m_Title;
        bool m_ForceCreateAsPage = false;
    };
}
