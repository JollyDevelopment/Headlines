#include "UserSubredditsContentProvider.h"
#include "util/ThreadWorker.h"
#include <utility>
#include "API/api_fwd.h"
#include "API/RedditAPI.h"
#include "UI/Widgets/SearchResultWidget.h"

namespace ui
{
    UserSubredditsContentProvider::UserSubredditsContentProvider()
            : m_OnClick(nullptr)
    {

    }

    std::vector<RedditContentListItemRef> UserSubredditsContentProvider::GetContentInternal(std::string& lastTimeStamp, util::ThreadWorker* worker)
    {
        std::vector<api::SubredditRef> subreddits = api::RedditAPI::Get()->GetUserSubreddits(100, lastTimeStamp, m_ReloadRequired);
        std::vector<RedditContentListItemRef> uiElements;

        for (const api::SubredditRef& subreddit : subreddits)
        {
            if (worker->IsCancelled())
            {
                return uiElements;
            }

            SearchResultWidgetRef subredditUI = std::make_shared<SearchResultWidget>(subreddit, m_OnClick);
            uiElements.push_back(subredditUI);
        }

        return uiElements;
    }

    std::string UserSubredditsContentProvider::GetFailedToLoadErrorMsg()
    {
        return "No subreddits found. Retry?";
    }

    void UserSubredditsContentProvider::SetItemClickHandler(std::function<void(const SearchResultWidget*)> onClick)
    {
        m_OnClick = std::move(onClick);
    }
}