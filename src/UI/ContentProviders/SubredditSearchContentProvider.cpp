#include <AppSettings.h>

#include <utility>
#include "SubredditSearchContentProvider.h"
#include "UI/Widgets/SearchResultWidget.h"
#include "UI/Pages/SearchPage.h"
#include "API/RedditAPI.h"
#include "Application.h"

namespace ui
{
    SubredditSearchContentProvider::SubredditSearchContentProvider(const SearchPage* searchPage)
            : m_Parent(searchPage)
            , m_OnClick(nullptr)
    {

    }

    std::vector<RedditContentListItemRef> SubredditSearchContentProvider::GetContentInternal(std::string& lastTimeStamp, util::ThreadWorker* worker)
    {
        std::vector<api::SubredditRef> subreddits = api::RedditAPI::Get()->GetSearchSubreddits(m_Parent->GetSearchString(), AppSettings::Get()->GetInt("num_posts_to_load", 5), lastTimeStamp);
        std::vector<RedditContentListItemRef> uiElements;
        for (const api::SubredditRef& subreddit : subreddits)
        {
            if (worker->IsCancelled())
            {
                return uiElements;
            }

            SearchResultWidgetRef postUI = std::make_shared<SearchResultWidget>(subreddit, m_OnClick);
            uiElements.push_back(postUI);
        }

        return uiElements;
    }

    std::string SubredditSearchContentProvider::GetFailedToLoadErrorMsg()
    {
        return "No results found. Retry?";
    }

    void SubredditSearchContentProvider::SetItemClickHandler(std::function<void(const SearchResultWidget*)> onClick)
    {
        m_OnClick = std::move(onClick);
    }
}