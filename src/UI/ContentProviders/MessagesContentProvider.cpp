#include "MessagesContentProvider.h"
#include "util/ThreadWorker.h"
#include <utility>
#include "API/api_fwd.h"
#include "API/RedditAPI.h"
#include "UI/Widgets/MessageWidget.h"

namespace ui
{
    MessagesContentProvider::MessagesContentProvider(MessagesType type)
        : m_Type(type)
    {

    }

    std::vector<RedditContentListItemRef> MessagesContentProvider::GetContentInternal(std::string& lastTimeStamp, util::ThreadWorker* worker)
    {
        std::vector<api::MessageRef> messages = m_Type == MessagesType::Inbox ?
                                                api::RedditAPI::Get()->GetMessages(10, lastTimeStamp) :
                                                api::RedditAPI::Get()->GetSentMessages(10, lastTimeStamp);
        std::vector<RedditContentListItemRef> uiElements;

        for (const api::MessageRef& message : messages)
        {
            if (worker->IsCancelled())
            {
                return uiElements;
            }

            MessageWidgetRef messageWidget = std::make_shared<MessageWidget>(message);
            uiElements.push_back(messageWidget);
        }

        return uiElements;
    }

    std::string MessagesContentProvider::GetFailedToLoadErrorMsg()
    {
        return "No messages found. Retry?";
    }
}