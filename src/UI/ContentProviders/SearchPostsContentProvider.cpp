#include <AppSettings.h>
#include "SearchPostsContentProvider.h"
#include "UI/Widgets/PostWidget.h"
#include "API/RedditAPI.h"
#include "UI/Pages/SearchPage.h"
#include "Application.h"

namespace ui
{
    SearchPostsContentProvider::SearchPostsContentProvider(const ui::SearchPage* searchPage)
        : m_Parent(searchPage)
    {}

    std::vector<RedditContentListItemRef> SearchPostsContentProvider::GetContentInternal(std::string& lastTimeStamp, util::ThreadWorker* worker)
    {
        std::vector<api::PostRef> posts = api::RedditAPI::Get()->GetSearchPosts(m_Parent->GetSearchString(), AppSettings::Get()->GetInt("num_posts_to_load", 5), lastTimeStamp);
        std::vector<RedditContentListItemRef> uiElements;
        for (const api::PostRef& post : posts)
        {
            if (worker->IsCancelled())
            {
                return uiElements;
            }

            PostWidgetRef postUI = std::make_shared<PostWidget>(post);
            uiElements.push_back(postUI);
        }

        return uiElements;
    }

    std::string SearchPostsContentProvider::GetFailedToLoadErrorMsg()
    {
        return "No results found. Retry?";
    }
}