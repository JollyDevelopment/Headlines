#include "MultiRedditContentProvider.h"
#include "API/RedditAPI.h"
#include "Application.h"
#include "AppSettings.h"
#include "util/ThreadWorker.h"
#include "UI/Widgets/PostWidget.h"

namespace ui
{
    MultiRedditContentProvider::MultiRedditContentProvider(const api::MultiRedditRef& multiReddit)
        : m_MultiReddit(multiReddit)
    {

    }

    std::vector<RedditContentListItemRef> MultiRedditContentProvider::GetContentInternal(std::string& lastTimeStamp, util::ThreadWorker* worker)
    {
        std::vector<::api::PostRef> posts = ::api::RedditAPI::Get()->GetMultiRedditPosts(m_MultiReddit, Application::Get()->GetMultiPostsSorting(), AppSettings::Get()->GetInt("num_posts_to_load", 5), lastTimeStamp);
        std::vector<RedditContentListItemRef> uiElements;
        for (const api::PostRef& post : posts)
        {
            if (worker->IsCancelled())
            {
                return uiElements;
            }

            PostWidgetRef postUI = std::make_shared<PostWidget>(post);
            uiElements.push_back(postUI);
        }

        return uiElements;
    }

    std::string MultiRedditContentProvider::GetFailedToLoadErrorMsg()
    {
        return "No posts found. Retry?";
    }
}