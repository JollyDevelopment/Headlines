#pragma once
#include "RedditContentProvider.h"

namespace ui
{
    class SearchResultWidget;
    class UserSearchContentProvider : public RedditContentProvider
    {
    public:
        UserSearchContentProvider(const SearchPage* searchPage);
        void SetItemClickHandler(std::function<void(const SearchResultWidget*)> onClick);
    private:
        virtual std::vector<RedditContentListItemRef> GetContentInternal(std::string& lastTimeStamp, util::ThreadWorker* worker) override;
        virtual std::string GetFailedToLoadErrorMsg() override;

        const SearchPage* m_Parent;

        std::function<void(const SearchResultWidget*)> m_OnClick;
    };
}
