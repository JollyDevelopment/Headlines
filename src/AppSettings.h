#pragma once

class AppSettings
{
public:
    static AppSettings* Get();
    AppSettings();

    bool UseLibSecret();

    bool GetBool(const std::string& name, bool defaultVal);
    void SetBool(const std::string& name, bool value);

    int GetInt(const std::string& name, int defaultVal);
    void SetInt(const std::string& name, int value);

private:
    void Load();
    void Save();
    std::string GetSettingsFilePath();

    Json::Value m_SaveData;
    std::string m_AssetsPath;
};
